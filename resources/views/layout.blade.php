<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="theme-color" content="#6e5cfe">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
</head>
<body>
<div id="preloader" class="align-items-center justify-content-center" style="z-index: 10000">
    <svg width="77px" height="77px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
         preserveAspectRatio="xMidYMid" class="lds-square">
        <g transform="translate(20 20)">
            <rect x="-15" y="-15" width="30" height="30" fill="#4568f6" transform="scale(0.868024 0.868024)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.4s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(50 20)">
            <rect x="-15" y="-15" width="30" height="30" fill="#4568f6" transform="scale(0.58394 0.58394)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.3s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(80 20)">
            <rect x="-15" y="-15" width="30" height="30" fill="#4568f6" transform="scale(0.332594 0.332594)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.2s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(20 50)">
            <rect x="-15" y="-15" width="30" height="30" fill="#4568f6" transform="scale(0.58394 0.58394)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.3s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(50 50)">
            <rect x="-15" y="-15" width="30" height="30" fill="#6e5cfe" transform="scale(0.332594 0.332594)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.2s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(80 50)">
            <rect x="-15" y="-15" width="30" height="30" fill="#6e5cfe" transform="scale(0.255226 0.255226)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.1s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(20 80)">
            <rect x="-15" y="-15" width="30" height="30" fill="#4568f6" transform="scale(0.332594 0.332594)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.2s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(50 80)">
            <rect x="-15" y="-15" width="30" height="30" fill="#6e5cfe" transform="scale(0.255226 0.255226)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="-0.1s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
        <g transform="translate(80 80)">
            <rect x="-15" y="-15" width="30" height="30" fill="#c681fe" transform="scale(0.496586 0.496586)">
                <animateTransform attributeName="transform" type="scale" calcMode="spline" values="1;1;0.2;1;1"
                                  keyTimes="0;0.2;0.5;0.8;1" dur="1s"
                                  keySplines="0.5 0.5 0.5 0.5;0 0.1 0.9 1;0.1 0 1 0.9;0.5 0.5 0.5 0.5" begin="0s"
                                  repeatCount="indefinite"></animateTransform>
            </rect>
        </g>
    </svg>
</div>
<nav class="fixed-top">
    <nav class="menu">
        <input type="checkbox" class="menu-open" name="menu-open" id="menu-open"/>
        <label class="menu-open-button" for="menu-open">
            <span class="hamburger hamburger-1"></span>
            <span class="hamburger hamburger-2"></span>
            <span class="hamburger hamburger-3"></span>
        </label>

        <a href="/" class="menu-item">
            <i class="fa fa-home" aria-hidden="true"></i>
            <span>Главная</span>
        </a>
        <a href="/services" class="menu-item">
            <i class="fa fa-handshake-o" aria-hidden="true"></i>
            <span>Услуги</span>
        </a>
        <a href="/portfolio" class="menu-item">
            <i class="fa fa-briefcase" aria-hidden="true"></i>
            <span>Портфолио</span>
        </a>
        <a href="/contacts" class="menu-item">
            <i class="fa fa-address-card" aria-hidden="true"></i>
            <span>Контакты</span>
        </a>
    </nav>
    <!-- filters -->
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
        <defs>
            <filter id="shadowed-goo">
                <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10"/>
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7"
                               result="goo"/>
                <feGaussianBlur in="goo" stdDeviation="3" result="shadow"/>
                <feColorMatrix in="shadow" mode="matrix" values="0 0 0 0 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 -0.2"
                               result="shadow"/>
                <feOffset in="shadow" dx="1" dy="1" result="shadow"/>
                <feComposite in2="shadow" in="goo" result="goo"/>
                <feComposite in2="goo" in="SourceGraphic" result="mix"/>
            </filter>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10"/>
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7"
                               result="goo"/>
                <feComposite in2="goo" in="SourceGraphic" result="mix"/>
            </filter>
        </defs>
    </svg>
</nav>

@yield('content')

<footer class="container-fluid" id="main-footer">
    <!-- Footer Links -->
    <div class="container text-center text-md-left">
        <!-- Grid row -->
        <div class="row">
            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 mr-auto my-md-4 my-0 mt-4 mb-1">
                <!-- Content -->
                <h5 class="font-weight-bold text-uppercase mb-4">О нас</h5>
                <p>
                    <strong>QOOB</strong> - это коллектив единомышленников, который собрался в уютном офисе с
                    одной целью – стать лучшими, но не на бумаге, не в рейтингах, а для наших клиентов.
                    У нас большие амбиции, и в то же время - мы профессионалы. Каждый наш клиент - в первую очередь
                    наш друг!
                </p>
            </div>
            <!-- Grid column -->
            <hr class="clearfix w-100 d-md-none">
            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 mx-auto my-md-4 my-0 mt-4 mb-1">
                <!-- Links -->
                <h5 class="font-weight-bold text-uppercase mb-4">Ссылки</h5>
                <ul class="list-unstyled">
                    <li>
                        <p>
                            <a href="/">Главная</a>
                        </p>
                    </li>
                    <li>
                        <p>
                            <a href="/services">Услуги</a>
                        </p>
                    </li>
                    <li>
                        <p>
                            <a href="/portfolio">Портфолио</a>
                        </p>
                    </li>
                    <li>
                        <p>
                            <a href="/contacts">Связаться с нами</a>
                        </p>
                    </li>
                </ul>
            </div>
            <!-- Grid column -->
            <hr class="clearfix w-100 d-md-none">
            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 mx-auto my-md-4 my-0 mt-4 mb-1">
                <!-- Contact details -->
                <h5 class="font-weight-bold text-uppercase mb-4">Контакты</h5>
                <ul class="list-unstyled">
                    <li>
                        <p>
                            <i class="fa fa-home mr-3"></i> Харьков, Украина <br>
                            ул. Плехановская 126/1</p>
                    </li>
                    <li>
                        <p>
                            <a href="mailto:info@qoob-studio.com"><i class="fa fa-envelope mr-3"></i>
                                info@qoob-studio.com</a></p>
                    </li>
                    <li>
                        <p>
                            <a href="tel:tel:+380953568336"><i class="fa fa-phone mr-3"></i>(095) 356 83 36</a></p>
                    </li>
                </ul>
            </div>
            <!-- Grid column -->
            <hr class="clearfix w-100 d-md-none">
            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 text-center mx-auto my-4">
                <!-- Social buttons -->
                <h5 class="font-weight-bold text-uppercase mb-4">Мы в соц.сетях</h5>
                <!-- Facebook -->
                <a class="social-ico" href="#" target="_blank">
                    <i class="fa fa-facebook"></i>
                </a>
                <!-- Twitter -->
                <a class="social-ico" href="#" target="_blank">
                    <i class="fa fa-twitter"></i>
                </a>
                <a class="social-ico" href="https://www.instagram.com/qoob_studio/" target="_blank">
                    <i class="fa fa-instagram"></i>
                </a>
            </div>
            <!-- Grid column -->
        </div>
        <!-- Grid row -->
    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© qoob, 2018 | Все права защищены
        <a href="http://qoob-studio.com/"> qoob-studio.com</a>
    </div>
    <!-- Copyright -->
</footer>

<!-- Scripts -->
<script src="{{ asset('js/loader.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/menu.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>

@if(!Request::is('/'))
<script src="{{ asset('js/footer.js') }}"></script>
@endif

<script>
    new WOW().init();
</script>
</body>
</html>