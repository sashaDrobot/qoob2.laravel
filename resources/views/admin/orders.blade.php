@extends('admin.layout')

@section('title', 'Orders')

@section('content')

    <div class="card">
        <div class="card-header">
            <h1 class="card-title">Orders</h1>
           {{-- <p class="card-text">новых - {{ $orders->new->count() }},
                в разработке - {{ $orders->accepted->count() }},
                выполнено - {{ $orders->completed->count() }}</p>--}}
        </div>
        <order-list-component></order-list-component>
    </div>


    {{--<div id="orders">
        <div class="card">
            <div class="card-header">
                <h1 class="card-title">Orders</h1>
                <p class="card-text">новых - {{ $orders->new->count() }},
                    в разработке - {{ $orders->accepted->count() }},
                    выполнено - {{ $orders->completed->count() }}</p>
            </div>
            <div class="card-body">
                <h3 class="card-title">
                    New orders
                </h3>

                --}}{{--<div class="row">
                    @if($orders->new->isEmpty())
                        <p class="ml-3 mt-2">empty...</p>
                    @else
                        @foreach($orders->new as $order)
                            <div class="col-md-6 col-lg-4 mt-2">
                                <div class="card">
                                    <div class="card-body">
                                        <p class="card-text">Заказчик: {{ $order->name }}</p>
                                        <p class="card-text">Номер: <a href="tel:{{ $order->phone }}"
                                                                       class="contacts-link">{{ $order->phone }}</a></p>
                                        <p class="card-text">Email: <a href="mailto:{{ $order->email }}"
                                                                       class="contacts-link">{{ $order->email }}</a></p>
                                        <p class="card-text">Сообщение: {{ $order->message }} </p>
                                        <p class="card-text">Дата: {{ date_format($order->created_at, 'd.m.Y h:i') }}</p>
                                        <form action="/admin/order/accept/{{ $order->id }}" method="POST"
                                              style="display: inline-block">
                                            @csrf
                                            <button type="submit" class="btn btn-success">Accept</button>
                                        </form>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#deleteOrder" id="{{ $order->id }}">Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>--}}{{--
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">
                    Orders in development
                </h3>

                --}}{{--<div class="row">
                    @if($orders->accepted->isEmpty())
                        <p class="ml-3 mt-2">empty...</p>
                    @else
                        @foreach($orders->accepted as $order)
                            <div class="col-md-6 col-lg-4 mt-2">
                                <div class="card">
                                    <div class="card-body">
                                        <p class="card-text">Заказчик: {{ $order->name }}</p>
                                        <p class="card-text">Номер: <a href="tel:{{ $order->phone }}"
                                                                       class="contacts-link">{{ $order->phone }}</a></p>
                                        <p class="card-text">Email: <a href="mailto:{{ $order->email }}"
                                                                       class="contacts-link">{{ $order->email }}</a></p>
                                        <p class="card-text">Сообщение: {{ $order->message }} </p>
                                        <p class="card-text">Дата: {{ date_format($order->created_at, 'd.m.Y h:m') }}</p>
                                        <form action="/admin/order/ready/{{ $order->id }}" method="POST"
                                              style="display: inline-block">
                                            @csrf
                                            <button type="submit" class="btn btn-primary">Ready</button>
                                        </form>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#deleteOrder" id="{{ $order->id }}">Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>--}}{{--
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">
                    Completed orders
                </h3>

               --}}{{--<div class="row">
                    @if($orders->completed->isEmpty())
                        <p class="ml-3 mt-2">empty...</p>
                    @else
                        @foreach($orders->completed as $order)
                            <div class="col-md-6 col-lg-4 mt-2">
                                <div class="card">
                                    <div class="card-body">
                                        <p class="card-text">Заказчик: {{ $order->name }}</p>
                                        <p class="card-text">Номер: <a href="tel:{{ $order->phone }}"
                                                                       class="contacts-link">{{ $order->phone }}</a></p>
                                        <p class="card-text">Email: <a href="mailto:{{ $order->email }}"
                                                                       class="contacts-link">{{ $order->email }}</a></p>
                                        <p class="card-text">Сообщение: {{ $order->message }} </p>
                                        <p class="card-text">Дата: {{ date_format($order->created_at, 'd.m.Y h:m') }}</p>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#deleteOrder" id="{{ $order->id }}">Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>--}}{{--
            </div>
        </div>

        <!-- Delete Order Modal -->
        --}}{{--<div class="modal fade" id="deleteOrder" tabindex="-1" role="dialog" aria-labelledby="deleteOrderLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteOrderLabel">Delete order</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="modal-text">Are you sure you want to transfer the order to the Recycle bin?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <form id="deleteOrderForm" action="/admin/order/delete" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-danger">Yes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>--}}{{--
    </div>--}}

@endsection