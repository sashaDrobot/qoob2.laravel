@extends('admin.layout')

@section('title', 'Users')

@section('content')

    <div class="card">
        <div class="card-header">
            <h1 class="card-title pt-2">
                Users
                <button id="addUserBtn" type="button" data-toggle="modal" data-target="#addUser">
                    <i class="fas fa-plus-circle"></i>
                </button>
            </h1>
        </div>
        <div class="card-body">
            <div class="row">
                @foreach($users as $user)
                    <div class="col-sm-6 col-md-4 col-xl-3 mt-2">
                        <div class="card">
                            <img class="card-img-top" src="{{ asset('img/default-user-profile.png') }}"
                                 alt="Card image user">
                            <div class="card-body">
                                <h5 class="card-title">{{ $user->name }}</h5>
                                <p class="card-text">{{ $user->email }}</p>
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#editUser" id="{{ $user->id }}">Edit
                                </button>
                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                        data-target="#deleteUser" id="{{ $user->id }}">Delete
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <!-- Add User Modal -->
    <div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="addUserLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addUserLabel">Add new user</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="addUserForm" action="/admin/users/add" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="inputName">Name</label>
                            <input type="text" class="form-control" id="inputName" name="name" placeholder="Enter name"
                                   required>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Email address</label>
                            <input type="email" class="form-control" id="inputEmail" name="email"
                                   aria-describedby="emailHelp" placeholder="Enter email" required>
                            <small id="emailHelp" class="form-text text-muted">Email должен быть уникальным.</small>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword">Password</label>
                            <input type="password" class="form-control" id="inputPassword" name="password"
                                   aria-describedby="passwordHelp" placeholder="Password" required>
                            <small id="passwordHelp" class="form-text text-muted">Пароль должен содержать не менее 6
                                символов.
                            </small>
                        </div>
                        <div class="alert alert-danger" id="addMessage">

                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit User Modal -->
    <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="editUserLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editUserLabel">Edit user</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-secondary" role="alert">
                        Заполните те поля, значение которых Вы хотите изменить, остальные - просто оставьте пустыми!
                    </div>
                    <form id="editUserForm" action="/admin/user/edit" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="editName">New name</label>
                            <input type="text" class="form-control" id="editName" name="newname"
                                   placeholder="Enter new name">
                        </div>
                        <div class="form-group">
                            <label for="editEmail">New email address</label>
                            <input type="email" class="form-control" id="editEmail" name="newemail"
                                   aria-describedby="editEmail" placeholder="Enter new email">
                            <small id="editEmail" class="form-text text-muted">Email должен быть уникальным.</small>
                        </div>
                        <div class="form-group">
                            <label for="editPassword">New password</label>
                            <input type="password" class="form-control" id="editPassword"
                                   aria-describedby="editPasswordHelp" name="newpassword"
                                   placeholder="Enter new password">
                            <small id="editPasswordHelp" class="form-text text-muted">Пароль должен содержать не менее 6
                                символов.
                            </small>
                        </div>
                        <div class="alert alert-danger" id="editMessage">

                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete User Modal -->
    <div class="modal fade" id="deleteUser" tabindex="-1" role="dialog" aria-labelledby="deleteUserLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteUserLabel">Delete user</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Are you sure you want to delete the user?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <form id="deleteUserForm" action="/admin/user/delete" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection