@extends('admin.layout')

@section('title', 'Admin')

@section('content')

    <div class="card">
        <div class="card-header">
            <h1 class="card-title pt-2">
                Welcome, {{ Auth::user()->name }}!
            </h1>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 col-lg-4 mt-2">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Orders</h4>
                            <p class="card-text">Новых заказов: {{ $orders->where('status','new')->count() }}</p>
                            <p class="card-text">В разработке: {{ $orders->where('status','accepted')->count() }}</p>
                            <p class="card-text">Выполненных заказов: {{ $orders->where('status','completed')->count() }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection