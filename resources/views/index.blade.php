@extends('layout')

@section('title', 'qoob | Главная')

@section('content')
    <div id="main-page">
        <div class="container-fluid main-container">
            <a href="#" id="play-btn" class="align-items-center justify-content-center">
                <i class="fa fa-play" aria-hidden="true"></i>
            </a>
            <div class="row d-flex align-items-center justify-content-center" id="main-blocks">
                <div class="left-main-block">
                    <div class="row">
                        <div class="col-12 main-headers text-left">
                            <h1>Qoob</h1>
                            <h1>studio</h1>
                            <div class="h-line">
                                <i class="fa fa-cube" aria-hidden="true"></i>
                            </div>
                            <h2>Твой</h2>
                            <h2>эксклюзивный</h2>
                            <h3>сайт</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <a href="/contacts" class="gradient-button text-center">
                                Заказать
                            </a>
                        </div>
                    </div>
                </div>
                <div class="right-main-block d-flex align-items-center justify-content-center">
                    <img src="img/hand_with_cube.png">
                    <h2 class="second-step-header">Создание сайта - это особый вид искусства</h2>
                    <h2 class="second-step-header">Мы - мастера своего дела</h2>
                    <h2 class="second-step-header">Каждый наш сайт поражает воображение</h2>
                    <h2 class="second-step-header">Давайте творить вместе!</h2>
                </div>
            </div>
        </div>
        <div id="main-content">
            <div class="container-fluid about-us-container">
                <div class="row py-5">
                    <div class="col-12 text-center py-4 container-header">
                        <h2>О нас</h2>
                        <div class="h-line">
                            <i class="fa fa-cube" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="col-lg-5 cube-wrapper">
                        <canvas id='canvas' width='600' height='600'></canvas>
                    </div>
                    <div class="col-lg-7 d-flex align-items-center justify-content-center">
                        <p>
                            <strong>QOOB</strong> - это коллектив единомышленников, который собрался в уютном офисе с
                            одной целью – стать лучшими, но не на бумаге, не в рейтингах, а для наших клиентов.
                            У нас большие амбиции, и в то же время - мы профессионалы. Каждый наш клиент - в первую очередь
                            наш друг!
                        </p>
                    </div>
                </div>
            </div>
            <div class="container-fluid serv-container">
                <div class="row py-5 align-items-center justify-content-center wow fadeInLeft">
                    <div class="col-12 text-center py-4 container-header">
                        <h2>Услуги</h2>
                        <div class="h-line">
                            <i class="fa fa-cube" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <p>Лендинг или крупномасштабное комлексное решение?</p>
                        <p>Сайт или веб-приложение?</p>
                        <p>Есть идеи?</p>
                        <p>Пора действовать!</p>
                        <p>Мы воплотим все ваши мечты в реальность!</p>
                    </div>
                    <div class="col-lg-5 cube text-center">
                        <img src="img/geometry-cube.png" class="img-fluid">
                    </div>
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <a href="/services" class="gradient-button text-center">
                            Подробнее
                        </a>
                    </div>
                </div>
            </div>
            <div class="container-fluid advantages-container py-5">
                <div class="row advantages-header">
                    <div class="col-12 text-center py-4 container-header">
                        <h2>После заказа сайта:</h2>
                        <div class="h-line">
                            <i class="fa fa-cube" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="row advantages-row">
                    <div class="col-lg-3 advantages-elem-wrapper d-flex align-items-center flex-column">
                        <img src="img/geometry-cube-2.png">
                        <span class="vl"></span>
                        <p class="text-center">Ваша компания упакована <br> в электронном виде</p>
                    </div>
                    <div class="col-lg-3 advantages-elem-wrapper d-flex align-items-center flex-column">
                        <img src="img/geometry-cube-3.png">
                        <span class="vl"></span>
                        <p class="text-center">Вы выделились <br> среди конкурентов</p>
                    </div>
                    <div class="col-lg-3 advantages-elem-wrapper d-flex align-items-center flex-column">
                        <img src="img/geometry-cube-4.png">
                        <span class="vl"></span>
                        <p class="text-center">О Вас говорят <br> больше</p>
                    </div>
                    <div class="col-lg-3 advantages-elem-wrapper d-flex align-items-center flex-column">
                        <img src="img/geometry-cube-5.png">
                        <span class="vl"></span>
                        <p class="text-center">Вам открыт новый путь <br> для продаж в сети</p>
                    </div>
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <a href="/contacts" class="gradient-button text-center">
                            Заказать
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/animations.js"></script>
    <script src="js/vector.js"></script>
    <script src="js/cube.js"></script>
    <script src="js/render.js"></script>
@endsection