@extends('layout')

@section('title', 'qoob | Контакты')

@section('content')
    <div class="container-fluid contacts-container" id="main-page">
        <div class="row align-items-center justify-content-center flex-column">
            <div class="col-12 text-center py-4 container-header colored-headers">
                <h2>Контакты</h2>
                <div class="h-line">
                    <i class="fa fa-cube" aria-hidden="true"></i>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 contacts-form">
                        <form class="text-center" action="/feedback" method="post" id="feedback">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="ВВЕДИТЕ ВАШЕ ИМЯ" name="name"
                                       autocomplete="newname" required>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="tel" placeholder="ВВЕДИТЕ ВАШ НОМЕР" id="phone"
                                       name="phone" autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="ВВЕДИТЕ ВАШ EMAIL"
                                       autocomplete="newemail" name="email" required>
                            </div>
                            <div class="form-group">
                                <select name="service" class="form-control">
                                    <option value="" selected disabled>ВИД УСЛУГИ</option>
                                    <option value="Landing page">Landing page</option>
                                    <option value="Сайт-визитка">Сайт-визитка</option>
                                    <option value="Интернет-магазин">Интернет-магазин</option>
                                    <option value="Корпоративный сайт">Корпоративный сайт</option>
                                    <option value="Другое">Другое</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" rows="3" placeholder="ОПИШИТЕ ВАШ ВОПРОС" name="message"
                                          required></textarea>
                            </div>
                            <div class="text-center send-btn-wrapper my-5">
                                <button type="submit" class="gradient-button" id="load">
                                    <span>Отправить</span>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 contacts-info d-flex align-items-center justify-content-center">
                        <!-- Contact details -->
                        <ul class="list-unstyled">
                            <li>
                                <p>
                                    <i class="fa fa-home mr-3"></i> Харьков, Украина <br>
                                    ул. Плехановская 126/1</p>
                            </li>
                            <li>
                                <p>
                                    <a href="mailto:info@qoob-studio.com"><i class="fa fa-envelope mr-3"></i>
                                        info@qoob-studio.com</a></p>
                            </li>
                            <li class="mb-5">
                                <p>
                                    <a href="tel:tel:+380953568336"><i class="fa fa-phone mr-3"></i>(095) 356 83 36</a>
                                </p>
                            </li>
                            <li class="mb-5">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2566.022959564245!2d36.27627811605097!3d49.973427979412406!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x41270b03479f59c3%3A0x5a1a6fcafef07b9!2sqoob-studio!5e0!3m2!1sru!2sua!4v1536153211732"
                                        width="100%" height="250" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#phone").mask("+38 (999) 999-9999");
            $('#feedback').submit(function(event) {
                event.preventDefault();
                $.ajax({
                    type: $(this).attr('method'),
                    url: $(this).attr('action'),
                    data: new FormData(this),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $('#load').html('<i class="fa fa-circle-o-notch fa-spin"></i> обработка').prop('disabled', true);
                    },
                    success: function(result) {
                        $('#load').html(result);
                    },
                    error: function () {
                        $('#load').html('Ошибка!');
                    },
                    complete: function () {
                        setTimeout(() => {
                            $('#load').html('Отправить').prop('disabled', false);
                        }, 3000);
                        $('#feedback input, #feedback textarea').val("");
                        $('#feedback select option:first-child').prop('selected', true);
                    }
                });
            });
        });
    </script>
@endsection