@extends('layout')

@section('title', 'qoob | Портфолио')

@section('content')
    <div class="container-fluid portfolio-container" id="main-page">
        <div class="port-bg">
            <div class="port-bg-elem">
            </div>
            <div class="port-bg-elem d-flex align-items-center justify-content-center">
                <span>q</span>
            </div>
            <div class="port-bg-elem d-flex align-items-center justify-content-center">
                <span class="wink">o</span>
            </div>
            <div class="port-bg-elem d-flex align-items-center justify-content-center">
                <span class="wink">o</span>
            </div>
            <div class="port-bg-elem d-flex align-items-center justify-content-center">
                <span>b</span>
            </div>
            <div class="port-bg-elem">
            </div>
        </div>
        <div class="row portfolio-row align-items-center justify-content-center animated">
            <div class="col-12 portfolio-elements center" id="portfolio-slider">
                <div class="portfolio-element d-flex align-items-center justify-content-center">
                    <a href="http://pitstopmda.com.ua/main" target="_blank">
                        <img src="http://qoob-studio.com/img/tiers.png" class="img-fluid">
                        <span>Шины и Диски</span>
                    </a>
                </div>
                <div class="portfolio-element d-flex align-items-center justify-content-center">
                    <a href="http://prominvest.com.ua/" target="_blank">
                        <img src="http://qoob-studio.com/img/prom.png" class="img-fluid">
                        <span>prominvest-plastic</span>
                    </a>
                </div>
                <div class="portfolio-element d-flex align-items-center justify-content-center">
                    <a href="http://nirvana.org.ua/" target="_blank">
                        <img src="img/nirvana-port.png" class="img-fluid">
                        <span>Nirvana-events</span>
                    </a>
                </div>
                <div class="portfolio-element d-flex align-items-center justify-content-center">
                    <a href="http://takt-avto.com.ua/" target="_blank">
                        <img src="img/takt-port.png" class="img-fluid">
                        <span>Такт Авто</span>
                    </a>
                </div>
                <div class="portfolio-element d-flex align-items-center justify-content-center">
                    <a href="http://station-hostel.com.ua" target="_blank">
                        <img src="img/hostel-station-port.png" class="img-fluid">
                        <span>Hostel Station</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('.center').slick({
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 3,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    </script>
@endsection