@extends('layout')

@section('title', 'qoob | Услуги')

@section('content')
    <div class="container-fluid services-container d-flex flex-column" id="main-page">
        <div class="row align-items-center justify-content-center" id="services-headers">
            <div class="col-12 text-center py-4 container-header">
                <h2>Мы не просто делаем сайты</h2>
                <h2>Мы решаем ряд бизнес-задач</h2>
                <div class="h-line">
                    <i class="fa fa-cube" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        <div class="port-bg">
            <div class="port-bg-elem">

            </div>
            <div class="port-bg-elem d-flex align-items-center justify-content-center">
                <span>q</span>
            </div>
            <div class="port-bg-elem d-flex align-items-center justify-content-center">
                <span class="wink">o</span>
            </div>
            <div class="port-bg-elem d-flex align-items-center justify-content-center">
                <span class="wink">o</span>
            </div>
            <div class="port-bg-elem d-flex align-items-center justify-content-center">
                <span>b</span>
            </div>
            <div class="port-bg-elem">

            </div>
        </div>
        <div class="row d-flex align-items-center justify-content-center align-self-center serv-row">
            <div class="col-12 text-center py-4 container-header colored-headers">
                <h2>Услуги</h2>
                <div class="h-line">
                    <i class="fa fa-cube" aria-hidden="true"></i>
                </div>
            </div>
            <div class="col-1 slick-control left-slick-control" style="margin-left: -2px;">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
            </div>
            <div class="col-10 services-main-slider">
                <div class="row services-row d-flex align-items-center justify-content-center">
                    <div class="col-12 text-center my-5 services-header">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h3>РАЗРАБОТКА WEB-САЙТОВ:</h3>
                            </div>
                            <div class="col-12 sub-services services-slider">
                                <div class="row d-flex flex-column align-items-center justify-content-center sub-serv-inner">
                                    <div class="col-md-6">
                                        <h4>LANDING PAGE</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Отличный выбор для продвижения продукта или услуги – призывает пользователя
                                            на
                                            совершение
                                            покупки или
                                            перехода на основной сайт. Может быть использован как вспомогательный
                                            инструмент для
                                            вашего
                                            основного сайта</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Сайт-визитка</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Лучше всего раскроет вас как личность, профессионала или вашу небольшую, но
                                            гордую
                                            компанию.
                                            Удобный, непростой и красивый – прямо как Вы.</p>
                                    </div>
                                </div>
                                <div class="row d-flex flex-column align-items-center justify-content-center sub-serv-inner">
                                    <div class="col-md-6">
                                        <h4>ИНТЕРНЕТ-МАГАЗИН</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Пора бы заняться бизнесом по серьезному? Чтобы Вы не задумали – нужно
                                            конкурировать.
                                            И именно функциональное и визуальное оформление играет немаловажную роль. Ну
                                            и идея,
                                            конечно же.</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h4>КОРПОРАТИВНЫЙ САЙТ</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Сделаем Вашу компанию вновь крутой! Давайте обновляться и идти в ногу со
                                            временем.
                                            Лучшее решение – когда Ваша компания будет иметь флагманский сайт среди
                                            конкурентов.</p>
                                    </div>
                                </div>
                                <div class="row d-flex flex-column align-items-center justify-content-center sub-serv-inner">
                                    <div class="col-md-6">
                                        <h4>ON-LINE СЕРВИСЫ</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Задумали создать бизнес в интернете, на полноценном сервисе в сети? Мы только
                                            рады
                                            присоединиться к крупномасштабному проекту и сделать его лучшим.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row services-row d-flex align-items-center justify-content-center">
                    <div class="col-12 text-center my-5 services-header">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h3>ПРОДВИЖЕНИЕ САЙТА:</h3>
                            </div>
                            <div class="col-12 sub-services services-slider">
                                <div class="row d-flex flex-column align-items-center justify-content-center sub-serv-inner">
                                    <div class="col-md-6">
                                        <h4>SEO ОПТИМИЗАЦИЯ</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Оптимизируем сайт и выводим в верхние позиции выдачи поиска, тем самым
                                            увеличивая
                                            посещаемость и, как следствие, продажи</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h4>КОНТЕКСТНАЯ РЕКЛАМА</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Создаем привлекательные рекламные объявления, направленные на вашу целевую
                                            аудиторию</p>
                                    </div>
                                </div>
                                <div class="row d-flex flex-column align-items-center justify-content-center sub-serv-inner">
                                    <div class="col-md-6">
                                        <h4>E-MAIL МАРКЕТИНГ</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Мы будем создавать и формировать правильные письма рассылки для Вашей базы
                                            клиентов</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h4>ЦЕЛЕВАЯ РЕКЛАМА В СОЦИАЛЬНЫХ СЕТЯХ</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Ваши клиенты будут идти к Вам прямиком из социальных сетей</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row services-row d-flex align-items-center justify-content-center">
                    <div class="col-12 text-center my-5 services-header">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h3>СЪЕМКА ВИДЕО:</h3>
                            </div>
                            <div class="col-12 sub-services services-slider">
                                <div class="row d-flex flex-column align-items-center justify-content-center sub-serv-inner">
                                    <div class="col-md-6">
                                        <h4>СЪЕМКА РЕКЛАМНЫХ ВИДЕО-РОЛИКОВ</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p>От идеи и концепции до монтажа и готового продукта. Обращаясь к нам, Вы
                                            получаете
                                            полный сервис разработки рекламного видео-продукта. Мы предлагаем креативные
                                            идеи,
                                            благодаря которым Ваша реклама запомнится. А может, и поедет в Канны.</p>
                                    </div>
                                </div>
                                <div class="row d-flex flex-column align-items-center justify-content-center sub-serv-inner">
                                    <div class="col-md-6">
                                        <h4>СЪЕМКА КОРПОРАТИВНЫХ ВИДЕО</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Лучше один раз увидеть что-то красивое, чем 100 раз увидеть скучное. Мы
                                            расскажем о
                                            компании потенциальным клиентам в формате ролика, где покажем Ваши
                                            конкурентные
                                            преимущества и придумаем столько изюминок, что получится буквально кекс.</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row services-row d-flex align-items-center justify-content-center">
                    <div class="col-12 text-center my-5 services-header">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h3>БРЕНДИНГ:</h3>
                            </div>
                            <div class="col-12 sub-services services-slider">
                                <div class="row d-flex flex-column align-items-center justify-content-center sub-serv-inner">
                                    <div class="col-md-6">
                                        <h4>РАЗРАБОТКА
                                            БРЕНД-БУКА</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p></p>
                                    </div>
                                    <div class="col-md-6">
                                        <h4>АНАЛИЗ РЫНКА И КОНКУРЕНТОВ</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p></p>
                                    </div>
                                </div>
                                <div class="row d-flex flex-column align-items-center justify-content-center sub-serv-inner">
                                    <div class="col-md-6">
                                        <h4>НЕЙМИНГ, СОЗДАНИЕ ЛОГОТИПА И ФИРМЕННОГО СТИЛЯ</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p></p>
                                    </div>
                                    <div class="col-md-6">
                                        <h4>ИССЛЕДОВАНИЯ ПРЕДПОЧТЕНИЙ ЦЕЛЕВОЙ АУДИТОРИИ</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p></p>
                                    </div>
                                </div>
                                <div class="row d-flex flex-column align-items-center justify-content-center sub-serv-inner">
                                    <div class="col-md-6">
                                        <h4>ПОСТРОЕНИЕ ДОЛГОСРОЧНОЙ СТРАТЕГИИ ПРОДВИЖЕНИЯ БРЕНДА</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-1 slick-control right-slick-control d-flex justify-content-end" style="margin-left: -25px;">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/servicesSlider.js') }}"></script>
    <script>
        $(window).on('load', function () {
            let $preloader = $('#preloader');
            let $loader = $preloader.find('svg');
            setTimeout(function () {
                $loader.fadeOut();
                $preloader.delay(350).fadeOut('slow');
                setTimeout(function () {
                    document.querySelector("#services-headers").style.top = "-150vh";
                    setTimeout(function () {
                        document.querySelector("#services-headers").style.display = "none";
                    }, 1000)
                }, 3000)
            }, 2000);
        });
    </script>
@endsection